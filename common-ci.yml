variables:
  MAVEN_OPTS: >-
    -Dhttps.protocols=TLSv1.2
    -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/${CI_COMMIT_REF_SLUG}-${CI_PIPELINE_ID}/repository
    -Dorg.slf4j.simpleLogger.showDateTime=true
    -Djava.awt.headless=true
  MAVEN_CLI_OPTS: >-
    --batch-mode
    --errors
    --fail-at-end
    --show-version
    --no-transfer-progress
    -DinstallAtEnd=true
    -DdeployAtEnd=true

image: registry.cern.ch/docker.io/library/maven:3.9.6-eclipse-temurin-21-alpine

stages:
  - code_quality
  - test
  - maven_build
  - post_build
  - docker_build
  - preview
  - deploy

cache:
  key: ${CI_COMMIT_REF_SLUG}-${CI_PIPELINE_ID}
  paths:
    - .m2/${CI_COMMIT_REF_SLUG}-${CI_PIPELINE_ID}/repository

before_script:
  - apk add --no-cache wget
  - wget -O ci_settings.xml https://gitlab.cern.ch/nile/java-build-tools/-/raw/master/ci_settings.xml || true

checkstyle:
  stage: code_quality
  script:
    - mvn --settings ci_settings.xml $MAVEN_CLI_OPTS checkstyle:checkstyle
    - 'echo Checkstyle Report available at: target/checkstyle-result.xml'
  artifacts:
    paths:
      - target/checkstyle-result.xml
    expire_in: 1 hour
    when: always

pmd:
  stage: code_quality
  script:
    - mvn --settings ci_settings.xml $MAVEN_CLI_OPTS pmd:check
    - mvn --settings ci_settings.xml $MAVEN_CLI_OPTS pmd:cpd-check
    - 'echo "PMD Report available at:: target/site/pmd.html"'
    - 'echo "CPD Report available at: target/site/cpd.html"'
  artifacts:
    paths:
      - target/site/
    expire_in: 1 hour
    when: always

test:
  stage: test
  needs: [ ]
  script:
    - mvn $MAVEN_CLI_OPTS test
  artifacts:
    paths:
      - target/surefire-reports/
    expire_in: 1 hour
    when: always

set_project_version:
  stage: maven_build
  needs: [ ]
  script:
    - VERSION=$(mvn -q -Dexec.executable=echo -Dexec.args='${project.version}' --non-recursive exec:exec)
    - if [ "$CI_COMMIT_BRANCH" == "qa" ]; then export VERSION="${VERSION}-QA"; elif [ "$CI_COMMIT_BRANCH" != "master" ]; then export VERSION="${VERSION}-${CI_COMMIT_BRANCH}-SNAPSHOT"; fi
    - mvn versions:set -DnewVersion=$VERSION
    - 'echo "$VERSION" > version.txt'
  artifacts:
    paths:
      - pom.xml
      - version.txt
    expire_in: 1 hour

build_artifacts:
  stage: maven_build
  needs:
    - checkstyle
    - pmd
    - set_project_version
    - test
  script:
    - mvn $MAVEN_CLI_OPTS package -DskipTests
  artifacts:
    paths:
      - target/*.jar
      - target/classes/

spotbugs:
  stage: post_build
  needs:
    - build_artifacts
  script:
    - mvn --settings ci_settings.xml $MAVEN_CLI_OPTS spotbugs:check
    - 'echo "Spotbugs Report available at: target/spotbugs.html"'
  artifacts:
    paths:
      - target/spotbugs.html
    expire_in: 1 hour
    when: always
