<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>ch.cern.nile</groupId>
    <artifactId>[library-name]</artifactId>
    <version>1.0.0</version>

    <properties>
        <maven.compiler.release>11</maven.compiler.release>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <kafka.version>2.0.0</kafka.version>
    </properties>

    <dependencies>
        <!-- other dependencies -->
        <dependency>
            <groupId>com.github.spotbugs</groupId>
            <artifactId>spotbugs-annotations</artifactId>
            <version>4.8.4</version>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <!-- Checkstyle plugin -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-checkstyle-plugin</artifactId>
                <version>3.3.1</version>
                <configuration>
                    <configLocation>
                        https://gitlab.cern.ch/nile/java-build-tools/-/raw/master/code-quality/checkstyle.xml?ref_type=heads
                    </configLocation>
                    <suppressionsLocation>
                        https://gitlab.cern.ch/nile/java-build-tools/-/raw/master/code-quality/checkstyle-suppressions.xml?ref_type=heads
                    </suppressionsLocation>
                    <consoleOutput>true</consoleOutput>
                    <failsOnError>true</failsOnError>
                    <linkXRef>false</linkXRef>
                </configuration>
            </plugin>

            <!-- PMD plugin -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-pmd-plugin</artifactId>
                <version>3.24.0</version>
                <dependencies>
                    <dependency>
                        <groupId>net.sourceforge.pmd</groupId>
                        <artifactId>pmd-core</artifactId>
                        <version>7.4.0</version>
                    </dependency>
                    <dependency>
                        <groupId>net.sourceforge.pmd</groupId>
                        <artifactId>pmd-java</artifactId>
                        <version>7.4.0</version>
                    </dependency>
                </dependencies>
                <configuration>
                    <linkXRef>false</linkXRef>
                    <rulesets>
                        <ruleset>
                            https://gitlab.cern.ch/nile/java-build-tools/-/raw/master/code-quality/pmd_java_ruleset.xml?ref_type=heads
                        </ruleset>
                    </rulesets>
                    <includeTests>true</includeTests>
                    <failOnViolation>true</failOnViolation>
                    <printFailingErrors>true</printFailingErrors>
                </configuration>
            </plugin>

            <!-- SpotBugs plugin -->
            <plugin>
                <groupId>com.github.spotbugs</groupId>
                <artifactId>spotbugs-maven-plugin</artifactId>
                <version>4.8.4.0</version>
                <configuration>
                    <effort>Max</effort>
                    <xmlOutput>false</xmlOutput>
                    <htmlOutput>true</htmlOutput>
                    <plugins>
                        <plugin>
                            <groupId>com.h3xstream.findsecbugs</groupId>
                            <artifactId>findsecbugs-plugin</artifactId>
                            <version>1.12.0</version>
                        </plugin>
                    </plugins>
                </configuration>
            </plugin>

            <!-- Compiler plugin -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.0</version>
            </plugin>

            <!-- Surefire plugin for testing -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>3.0.0-M5</version>
            </plugin>

            <!-- Source plugin -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <version>3.3.0</version>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <!-- Javadoc plugin -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>3.6.3</version>
                <executions>
                    <execution>
                        <id>generate-javadocs</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                        <phase>package</phase>
                    </execution>
                </executions>
                <configuration>
                    <show>private</show>
                    <charset>UTF-8</charset>
                    <docencoding>UTF-8</docencoding>
                    <useStandardDocletOptions>true</useStandardDocletOptions>
                    <footer><![CDATA[<a href="https://gitlab.cern.ch/nile/streams/libs/[library-name]">View Source on Repository</a>]]></footer>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <repositories>
        <repository>
            <id>gitlab-maven</id>
            <url>${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/maven</url>
        </repository>
    </repositories>

    <pluginRepositories>
        <pluginRepository>
            <id>apache.snapshots</id>
            <name>Apache Snapshot Repository</name>
            <url>https://repository.apache.org/snapshots</url>
            <releases>
                <enabled>false</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </pluginRepository>
    </pluginRepositories>

    <distributionManagement>
        <repository>
            <id>gitlab-maven</id>
            <url>${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/maven</url>
        </repository>
        <snapshotRepository>
            <id>gitlab-maven</id>
            <url>${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/maven</url>
        </snapshotRepository>
    </distributionManagement>

</project>
